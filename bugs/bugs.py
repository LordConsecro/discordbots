from discord.ext import commands
from cogs.utils.dataIO import fileIO
from cogs.utils import checks
from __main__ import send_cmd_help
import os

class Tickets:
    def __init__(self, bot):
        self.bot = bot
        self.bugs = fileIO("data/bugs/bugs.json", "load")
        self.settings = fileIO("data/bugs/settings.json", "load")

    @property
    def bug_limit(self):
        num = self.settings.get("BUGS_PER_USER", -1)
        if num == -1:
            self.bug_limit = 0
            num = 0
        return num

    @bug_limit.setter
    def bug_limit(self, num):
        self.settings["BUGS_PER_USER"] = num
        fileIO("data/bugs/settings.json", "save", self.settings)

    @property
    def keep_on_read(self):
        ret = self.settings.get("KEEP_ON_READ")
        if ret is None:
            self.keep_on_read = False
            ret = False
        return ret

    @keep_on_read.setter
    def keep_on_read(self, value):
        self.settings["KEEP_ON_READ"] = bool(value)
        fileIO("data/bugs/settings.json", "save", self.settings)

    @property
    def reply_to_user(self):
        ret = self.settings.get("REPLY_TO_USER")
        if ret is None:
            ret = False
            self.reply_to_user = ret
        return ret

    @reply_to_user.setter
    def reply_to_user(self, val):
        self.settings["REPLY_TO_USER"] = val
        fileIO("data/bugs/settings.json", "save", self.settings)

    def _get_bug(self):
        if len(self.bugs) > 0:
            bug = self.bugs[0]
            for idnum in bug:
                ret = bug[idnum].get(
                    "name", "no_name") + ": " + \
                    bug[idnum].get("message", "no_message")
            if not self.keep_on_read:
                self.bugs = self.bugs[1:]
                fileIO("data/bugs/bugs.json", "save", self.bugs)
            return ret
        else:
            return "No more bugs!"

    def _get_bugs(self):
        if len(self.bugs) > 0:
            bugcount = len(self.bugs)
            ret = ""
            bi = 0
            while bi < bugcount:
                bug = self.bugs[bi]
                for idnum in bug:
                    ret += bug[idnum].get(
                        "name", "no_name") + ": " + \
                        bug[idnum].get("message", "no_message") + "\n"
                bi += 1
            return ret
        else:
            return "No bugs!"

    def _get_number_bugs(self, author):
        idnum = author.id
        num = len([x for bug in self.bugs for x in bug if x == idnum])
        return num

    def _add_bug(self, author, message):
        self.bugs.append(
            {author.id: {"name": author.name, "message": message}})
        fileIO("data/bugs/bugs.json", "save", self.bugs)


    @commands.command(pass_context=True)
    async def bug(self, ctx, *, message):
        """Adds bug.

           Example: !bug The main door at diamond city doesnt open"""
        if self.bug_limit != 0 and \
                self._get_number_bugs(ctx.message.author) >= \
                self.bug_limit:
            await self.bot.say("{}, you've reached the bug limit!".format(
                ctx.message.author.mention))
            return
        self._add_bug(ctx.message.author, message)
        await self.bot.say("{}, bug added.".format(
            ctx.message.author.mention))


    @commands.group(pass_context=True)
    @checks.mod_or_permissions(manage_messages=True)
    async def bugadmin(self, ctx):

        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx)
            msg = "```"
            for k, v in self.settings.items():
                msg += str(k) + ": " + str(v) + "\n"
            msg += "```"
            await self.bot.say(msg)

    @bugadmin.command(name="bugs", pass_context=True)
    async def all_bugs(self):
        """Gets all bugs"""
        await self.bot.say(self._get_bugs())

    @bugadmin.command(name="current", pass_context=True)
    async def current_bug(self):
        """Gets current bug"""
        await self.bot.say(self._get_bug())

    @bugadmin.command(name="clear")
    async def clear_bugs(self):
        """Clears all bugs"""
        self.bugs = []
        fileIO("data/bugs/bugs.json", "save", self.bugs)
        await self.bot.say("bugs cleared.")


    @bugadmin.command(name="delete", pass_context=True)
    async def delete_bug(self, ctx, num: int=1):
        """Deletes any number of bugs, default = 1"""
        if num < 0:
            await send_cmd_help(ctx)
            return
        if num > len(self.bugs):
            num = len(self.bugs)
            self.bugs = []
        else:
            self.bugs = self.bugs[num:]
        fileIO("data/bugs/bugs.json", "save", self.bugs)
        await self.bot.say("{} bugs deleted.\n{} bugs remaining.".format(
            num, len(self.bugs)))

    @bugadmin.command(name="limit", pass_context=True)
    async def bugs_per_user(self, ctx, num: int):
        """Limits the number of bugs a user can have 0 = infinite."""
        if num < 0:
            await send_cmd_help(ctx)
            return
        self.settings["BUGS_PER_USER"] = num
        fileIO("data/bugs/settings.json", "save", self.settings)
        await self.bot.say("bugs per user set to {}".format(num))

    @bugadmin.command(name="keep", pass_context=True)
    async def _keep_on_read(self, ctx, val: bool):
        """Determines whether the bug is kept after it has been read.
         - True/False"""
        self.keep_on_read = val
        await self.bot.say("Keep on read set to {}".format(val))

    @bugadmin.command(name="pm")
    async def reply_to(self, boolvar: bool):
        """Determines whether !nextbug replies in a pm or not
         - True/False"""
        if boolvar:
            self.reply_to_user = True
        else:
            self.reply_to_user = False
        await self.bot.say("PM set to {}".format(boolvar))


def check_folder():
    if not os.path.exists("data/bugs"):
        print("Creating data/bugs folder...")
        os.makedirs("data/bugs")


def check_file():
    bugs = []
    settings = {"BUGS_PER_USER": 1,
                "REPLY_TO_USER": False, "KEEP_ON_READ": False}

    f = "data/bugs/bugs.json"
    if not fileIO(f, "check"):
        print("Creating default bugs's bugs.json...")
        fileIO(f, "save", bugs)

    f = "data/bugs/settings.json"
    if not fileIO(f, "check"):
        print("Creating default bugs's settings.json...")
        fileIO(f, "save", settings)


def setup(bot):
    check_folder()
    check_file()
    n = Tickets(bot)
    bot.add_cog(n)
